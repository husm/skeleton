<?php
define('COL_DESCRIPTION', 0);
define('COL_HELP', 1);
define('COL_DEFAULT', 2);

$fields = [
    'titulo_modulo' => [
        'Título, legível para humanos',
        'Exemplo: Relatórios',
        function ($values) {
            return getDefaultTitulo($values);
        }
    ],
    'nome_modulo' => [
        'Nome, como utilizado em namespaces',
        'Exemplo: Relatorios',
        function ($values) {
            return getDefaultNome($values);
        }
    ],
    'slug_modulo' => [
        'Slug',
        'Exemplo: relatorios',
        function ($values) {
            return getDefaultSlug($values);
        }
    ],
    'descricao_modulo' => [
        'Descrição curta do módulo',
        '',
        function ($values) {
            return $values['titulo_modulo'];
        }
    ],
    'nome_autor' => [
        'Nome do autor',
        '',
        'Equipe DEV HUSM'
    ],
    'email_autor' => [
        'E-mail do autor',
        '',
        'informatica@ufsm.br'
    ]
];

$values = [];

$replacements = [
    ':titulo_modulo' => function () use (&$values) {
        return $values['titulo_modulo'] ?: $values['titulo_modulo'] = getDefaultTitulo($values);
    },
    ':nome_modulo' => function () use (&$values) {
        return $values['nome_modulo'] ?: $values['nome_modulo'] = getDefaultNome($values);
    },
    ':slug_modulo' => function () use (&$values) {
        return $values['slug_modulo'] ?: $values['slug_modulo'] = getDefaultSlug($values);
    },
    ':descricao_modulo' => function () use (&$values) {
        return $values['descricao_modulo'] ?: $values['descricao_modulo'] = $fields['descricao_modulo'][COL_DEFAULT]($values);
    },
    ':nome_autor' => function () use (&$values, $fields) {
        return $values['nome_autor'] ?: $values['nome_autor'] = $fields['nome_autor'][COL_DEFAULT];
    },
    ':email_autor' => function () use (&$values, $fields) {
        return $values['email_autor'] ?: $values['email_autor'] = $fields['email_autor'][COL_DEFAULT];
    }
];

function getDefaultTitulo($values)
{
    return ucwords(pathinfo(__DIR__, PATHINFO_BASENAME));
}

function getDefaultNome($values)
{
    $nome = $values['titulo_modulo'];
    $nome = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $nome);
    $nome = ucwords($nome);
    return str_replace(' ', '', $nome);
}

function getDefaultSlug($values)
{
    return strtolower($values['nome_modulo']);
}

function read_from_console($prompt)
{
//    if (function_exists('readline')) {
//        $line = trim(readline($prompt));
//        if (!empty($line)) {
//            readline_add_history($line);
//        }
//    } else {
        echo $prompt;
        $line = trim(fgets(STDIN));
//    }
    return $line;
}

function rsearch($folder, $pattern)
{
    $dir = new RecursiveDirectoryIterator($folder);
    $ite = new RecursiveIteratorIterator($dir);
    $files = new RegexIterator($ite, $pattern, RegexIterator::GET_MATCH);
    $fileList = array();
    foreach ($files as $file) {
        $fileList = array_merge($fileList, $file);
    }
    return array_unique($fileList);
}

$modify = 'n';
do {
    if ($modify == 'q') {
        exit;
    }

    $values = [];

    echo "\033[44m                                                \033[0m\n";
    echo "\033[44m  Por favor, forneça as seguintes informações:  \033[0m\n";
    echo "\033[44m                                                \033[0m\n";
    echo "\n";

    foreach ($fields as $f => $field) {
        $default = is_callable($field[COL_DEFAULT]) ? $field[COL_DEFAULT]($values) : $field[COL_DEFAULT];
        $prompt = sprintf(
            "\033[0;32m%s\033[0;37m%s\033[1;30m%s:\033[0m ",
            $field[COL_DESCRIPTION],
            $field[COL_HELP] ? ' (' . $field[COL_HELP] . ')': '',
            $field[COL_DEFAULT] !== '' ? ' [' . $default . ']': ''
        );
        $values[$f] = read_from_console($prompt);
        if (empty($values[$f])) {
            $values[$f] = $default;
        }
    }
    echo "\n";

    echo "\033[44m                                                          \033[0m\n";
    echo "\033[44m  Por favor, verifique se as informações estão corretas:  \033[0m\n";
    echo "\033[44m                                                          \033[0m\n";
    echo "\n";

    foreach ($fields as $f => $field) {
        echo "\033[0;32m" . $field[COL_DESCRIPTION] . ":\033[0m $values[$f]\n";
    }
    echo "\n";
} while (($modify = strtolower(read_from_console('Modificar arquivos com estes valores? [y/N/q] '))) != 'y');
echo "\n";


$files = array_merge(
    glob(__DIR__ . '/*.md'),
    glob(__DIR__ . '/composer.json'),
    glob(__DIR__ . '/package.json'),
    rsearch(__DIR__, '/(^.*\.php$)/')
);

$files = array_filter($files, function ($e) {
    return ($e !== __FILE__);
});

foreach ($files as $f) {
    $contents = file_get_contents($f);
    foreach ($replacements as $str => $func) {
        $contents = str_replace($str, $func(), $contents);
    }
    file_put_contents($f, $contents);
    echo "Arquivo \033[0;32m" . $f . "\033[0m modificado.\n";
}

$filename = basename(__FILE__);
$fill = str_repeat(" ", strlen($filename));
echo "\033[44m                                         " . $fill . "\033[0m\n";
echo "\033[44m  Pronto.                                " . $fill . "\033[0m\n";
echo "\033[44m  Agora você deve remover o arquivo '" . basename(__FILE__) . "'.  \033[0m\n";
echo "\033[44m                                         " . $fill . "\033[0m\n";

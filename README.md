# Skeleton para módulos do AGINFO

Este projeto é um template para criação de novos módulos para o AGINFO.


## Iniciando

Crie a pasta onde será desenvolvido o novo módulo, clone o repositório para esta pasta e remova o git dela:
```shell
$ mkdir dev-packages/aginfo/<nome do módulo>
$ git clone git@dvl-ww6.ad.husm.ufsm.br:husm/aginfo-skeleton.git dev-packages/aginfo/<nome do módulo>
$ rm -rf dev-packages/aginfo/<nome do módulo>/.git
```

Use o script prefill.php para preencher as informações básicas do módulo:
```shell
$ php prefill.php
```
Este script perguntará por informações que serão usadas para alterar os arquivos do módulo, evitando trabalho manual.

Edite o arquivo composer.json do projeto principal para que o Laravel consiga achar seu novo módulo.
```json
"repositories": [
    //...
    {
        "type": "path",
        "url": "./dev-packages/aginfo/<nome do módulo>"
    },
],
//...
"require": {
    //...
    "aginfo/<nome do módulo>": "dev-master"
},
```

## Providers

Por padrão, o módulo possui 5 service providers. Eles se encontram na pasta `src/Providers`.
Você pode adicionar/remover providers conforme for necessário. Lembre-se de registrá-los e
removê-los na propriedade `extra.laravel.providers` no arquivo `composer.json` do módulo.

Os service providers padrão são:

## ModuleServiceProvider
Este service provider deve extender a classe `Aginfo\Base\Providers\AbstractModuleServiceProvider`.
Nele são informados os dados do módulo, bem como as permissões que ele inclui no sistema. Os dados
e as permissões são informados nas propriedades da classe: `$nome`, `$namespace`, `$key`, `$path` e
`$permissoes`. O script `prefill.php` preenche estas permissões para você.

A propriedade `$permissoes` é um array associativo, onde a chave é o identificador e o valor é o nome descritivo da 
permissão. Veja no arquivo um exemplo de permissões, as quais devem ser substituídas por valores reais.

## AuthServiceProvider
Aqui é implementada a lógica de validação das permissões informadas no provider acima. Também são
registradas as [policies](https://laravel.com/docs/5.8/authorization#creating-policies), quando utilizadas.
O arquivo possui código de exemplo de como validar uma permissão.

## RouteServiceProvider
A função deste provider é registrar as rotas para o módulo definidas em `routes/web.php` e `routes/api.php`.
Este arquivo não precisa ser alterado.

## BroadcastServiceProvider
Este provider registra as regras de autorização de canais de 
[broadcast](https://laravel.com/docs/5.8/broadcasting#concept-overview) definidas no arquivo `routes/channels.php`.

Caso o módulo não faça broadcast de eventos, este provider pode ser removido, bem como o arquivo `routes/channels.php`.

## EventServiceProvider
Aqui são registrados os eventos do módulo, conforme descrito na
[documentação do Laravel](https://laravel.com/docs/5.8/events).

## ScheduleServiceProvider
Este provider fornece o método `schedule`, onde podem ser feitos os agendamentos de tarefas, conforme explicado na
[documentação do Laravel](https://laravel.com/docs/5.8/scheduling). A diferença é que este provider é utilizado no lugar
da classe `App\Console\Kernel`.

Caso o módulo não utilize este recurso, este provider pode ser removido, bem como sua entrada em
no caminho `extra.laravel.providers` do arquivo `composer.json`.

## Rotas
As rotas são definidas no arquivo `routes/web.php`. Duas rotas de exemplo já estão definidas: uma sem bloqueios e outra
protegida por permissão.

Rotas podem ser protegidas utilizando-se o middleware `can`, passando-se o a chave do módulo + `:` + o nome da permissão
que o usuário logado deve possuir. Por exemplo:
```php
Route::get('/setores', 'SetoresController@index')
    ->name('aginfo.base:setores')
    ->middleware('can:aginfo.base:ger_setores');
```

Também está disponível o middleware `can.any`, o qual permite o acesso caso o usuário logado possua qualquer uma das
permissões informadas, separadas por vírgula. Por exemplo:
```php
Route::get('lista', 'CeController@lista')
    ->name('lista')
    ->middleware(
        'can.any:'.
        'aginfo.pacientes:ce_ver_lista,'.
        'aginfo.pacientes:ce_ger_lista'
    );
```

## Navegação
Os itens de navegação são definidos no arquivo `resources/nav.php`. O arquivo retorna um array onde cada elemento é um
item de menu.

Para cada item, a chave é um identificador do menu onde o item será inserido, e o valor é um array
com informações sobre o item:

A chave é uma sequência de segmentos separados por pontos.
O primeiro segmento pode ser `nav` ou `nav-right`, indicando se o item será colocado no menu direito ou esquerdo.
Os demais segmentos identificam o caminho no menu onde o item será inserido. Isto possibilita que um módulo possa
adicionar itens de menu como subitens de menus criados por outros módulos, bastando indicar o caminho.

O valor do item é um array no seguinte formato:
```php
[
     // O rótulo do item;
    'title' => '',

     // Um array de atributos HTML;
    'attributes' => [

         // A classe que o item terá. Usado para definir o ícone;
        'class' => 'icon-nurse-lock'
    ],

     // OPCIONAL;
    'options' => [

         // OPCIONAL. A URL para onde o link apontará;
        'link' => route('nome.da.rota'),

         // OPCIONAL. A permissão que o usuário precisa ter para ver este item.
         // Pode ser uma string ou array de strings. Caso seja um array, o acesso
         // será concedido se o usuário possuir qualquer uma das permissões informadas.
        'permissions' => 'aginfo.modulo:gerenciar_dados'
    ]
];
```

Veja como exemplo o código do arquivo nav.php do módulo base.


## Assets & Laravel Mix
Pode-se utilizar o Laravel Mix normalmente, conforme informado na [documentação do Laravel](https://laravel.com/docs/5.8/mix).

Os assets são compilados para a pasta `assets` do módulo com o comando abaixo:
```shell
$ yarn run dev
```

Os assets poderão ser acessados após ser executado o comando `php artisan aginfo:link-assets`.

## Database (migrations, factories, seeds)
As migrations, factories e seeds devem ser criadas nas respectivas pastas, dentro da pasta `database`. Elas podem ser
utilizadas normalmente como descrito na documentação do Laravel.

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/:slug_modulo', 'IndexController@index')
    ->name('aginfo.:slug_modulo:index');


Route::get('/:slug_modulo/protegido', 'IndexController@protegido')
    ->name('aginfo.:slug_modulo:protegido')
    ->middleware('can:aginfo.:slug_modulo:permissao_exemplo');

<?php

namespace Aginfo\:nome_modulo\Models;

use Illuminate\Database\Eloquent\Model;

class Exemplo extends Model
{
    protected $table = ':slug_modulo_exemplos';
}

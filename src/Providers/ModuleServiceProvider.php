<?php

namespace Aginfo\:nome_modulo\Providers;

use Illuminate\Support\Facades\Auth;
use Aginfo\Base\Providers\AbstractModuleServiceProvider;

class ModuleServiceProvider extends AbstractModuleServiceProvider
{
    /**
     * Indica se o carregamento do provider é atrasado.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Indica o nome do módulo.
     *
     * @var string
     */
    protected $nome = ':titulo_modulo';

    /**
     * Indica o namespace do módulo.
     *
     * @var string
     */
    protected $namespace = "Aginfo\:nome_modulo\\";

    /**
     * Indica a chave utilizada para identificar o módulo em arquivos de tradução,
     * arquivos de configuração, views e outros.
     *
     * @var string
     */
    protected $key = 'aginfo.:slug_modulo';

    /**
     * Indica o caminho de arquivos publicados pelo módulo e também dos assets
     * públicos.
     *
     * @var string
     */
    protected $path = 'aginfo/:slug_modulo';

    /**
     * Pares chave-descrição das permissões fornecidas pelo módulo.
     * Os valores informados são exemplos e devem ser alterados para valores reais.
     *
     * @var array
     */
    protected $permissoes = [
        'permissao_exemplo'  => 'Permissão Exemplo',
        'ger_dados' => 'Gerenciar Dados'
    ];

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        // Insira seu código aqui.
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}

<?php

namespace Aginfo\:nome_modulo\Providers;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(Gate $gate)
    {
        $this->registerPolicies();

        $this->defineAbilities($gate);
    }

    /**
     * Define as habilidades do módulo.
     *
     * Não é necessário checar se o usuário logado possui um perfil com a permissão informada.
     * Esta checagem já é realizada no método `before` da classe Gate.
     * Ver o método Aginfo\Base\Providers\AuthServiceProvider@registerPermissonsCheck.
     *
     * @see \Aginfo\Base\Providers\AuthServiceProvider@registerPermissionsCheck
     * @see https://laravel.com/docs/5.8/authorization#gates
     * @param  Illuminate\Support\Facades\Gate   $gate
     * @return void
     */
    protected function defineAbilities(Gate $gate)
    {
        // 'permissao_exemplo' é obviamente um exemplo e deve ser alterado.
        $gate->define('aginfo.:slug_modulo:permissao_exemplo', function ($user) {
            // Caso seja necessário executar alguma lógica extra para verificar se
            // o usuário tem acesso ao recurso, esta lógica deve ser executada aqui.
            // Um valor booleano deve ser retornado.
            return true;
        });
    }
}

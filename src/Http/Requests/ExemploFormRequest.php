<?php

namespace Aginfo\:nome_modulo\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExemploFormRequest extends FormRequest
{
    /**
     * Determina se o usuário está autorizado a fazer a requisição.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Pega as regras de validação que se aplicam à requisição.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Customiza os nomes dos campos para exibição nas mensagens de erro.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'data_de_nascimento' => 'Data de nascimento'
        ];
    }

    /**
     * Customiza as mensagens de erro retornadas.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'data_de_nascimento.required' => 'Que dia você nasceu mesmo?',
            'date_format' => 'O campo :attribute não está formatado como data.'
        ];
    }
}

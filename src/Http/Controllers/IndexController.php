<?php

namespace Aginfo\:nome_modulo\Http\Controllers;

use Illuminate\Routing\Controller;

class IndexController extends Controller
{
    public function index()
    {
        return view('aginfo.:slug_modulo:index');
    }

    public function protegido()
    {
        return 'Esta é uma rota protegida';
    }
}

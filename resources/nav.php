<?php

return [

    'nav.:slug_modulo' => [
        'title' => ':titulo_modulo',
        'attributes' => [ 'class' => 'icon-nurse-lock' ]
    ],

    'nav.:slug_modulo.subnivel' => [
        'title' => 'Subnível',
        'options' => [
            'link' => route('aginfo.:slug_modulo:exemplo'),
            'permissions' => 'aginfo.:slug_modulo:permissao_exemplo',
        ],
        'attributes' => [ 'class' => 'icon-license-key' ]
    ]
];

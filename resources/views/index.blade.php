@extends('aginfo.base::layouts.master')

@section('title', ':titulo_modulo')

@section('head')
  {{-- Inclua aqui as folhas de estilo da view --}}
  <link href="{{ asset('vendor/aginfo/:slug_modulo/css/modulo.css') }}" rel="stylesheet">
@endsection

@section('footer')
  {{-- Inclua aqui os javascripts da view --}}
  <script src="{{ asset('vendor/aginfo/:slug_modulo/js/modulo.js') }}"></script>
@endsection

@section('content')

  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id elit ante. Donec nec sem eget nunc venenatis tincidunt. Duis quis tortor ut ipsum finibus pulvinar at vitae nulla. Curabitur lectus nibh, pulvinar a tempus at, fringilla pretium eros. Praesent et leo placerat, euismod enim vel, accumsan sem. Vivamus vitae interdum est. Maecenas non tellus quis mauris porta varius. Nam nulla neque, hendrerit sit amet augue a, imperdiet malesuada arcu. Praesent sagittis euismod nisi et hendrerit. Vestibulum posuere dignissim nunc eu cursus.</p>
  <p>Nunc iaculis lorem urna, a lobortis libero tempor vitae. Pellentesque sollicitudin porta lectus vel interdum. Sed sed dui sed eros sollicitudin cursus vitae id ante. Praesent sit amet vulputate tellus. Quisque maximus varius est non varius. Donec interdum rhoncus commodo. Aliquam magna est, volutpat ut quam at, semper dictum lectus. Cras ullamcorper ligula sit amet pretium ornare.</p>

@endsection